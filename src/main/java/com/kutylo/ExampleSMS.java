package com.kutylo;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    // Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = "AC5129ffc31b3047104aed6a7c3db2ec72";
    public static final String AUTH_TOKEN = "55b2b91aa29069fc5ebef89b5fae3591";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(new PhoneNumber("+380971193319"), /*my phone number*/
                new PhoneNumber("+19384440642"), str).create(); /*attached to me number*/
    }
}
